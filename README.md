﻿delmarbo

\## Что такое git bash?

Git Bash-это **приложение, которое обеспечивает работу командной строки Git в операционной системе**. Это оболочка командной строки для включения git с помощью командной строки в системе. Оболочка-это терминальное приложение, используемое для взаимодействия с операционной системой с помощью письменных команд.

Bash — аббревиатура от Bourne Again Shell. Оболочка (Shell) представляет собой приложение терминала для взаимодействия с операционной системой с помощью письменных команд. Git Bash представляет собой пакет, который устанавливает в операционную систему Windows оболочку Bash, некоторые распространенные утилиты Bash и систему Git.

\## Что делает команда git pull? Чем отличается команда git pull от git push?

**Команда** **git** **pull** — это автоматизированная версия **команды** **git** fetch. **Она** загружает ветку из удаленного репозитория и сразу же объединяет **ее** с текущей веткой. 

**Git push** - команда используется в системе Git для выгрузки содержимого локального репозитория. Она позволяет передать коммиты из локального репозитория в удаленный. **Команда** **git** **push** противоположна **команде** извлечения. С **ее** помощью можно перенести локальную ветку в другой репозиторий и без труда опубликовать поступивший код.

\## Что такое merge request?

**Merge** **requests** — это запрос на слияние веток, основной способ внесения изменений в код при использовании GitLab. Изменения вносятся в код, затем автором изменений создаётся **merge** **request**, который затем обсуждается, в котором происходит code review. **Merge** **request** в результате принимается, отправляется на доработку или отклоняется.

\## Чем между собой отличаются команды git status и git log?

**Команда** **git** **log** отображает отправленные снимки состояния и позволяет просматривать и фильтровать историю проекта, а также искать в ней конкретные изменения. 

С помощью **git** **status** можно просматривать рабочий каталог и раздел проиндексированных файлов, в то время как **git** **log** показывает только историю коммитов.

\## Что такое submodule, с помощью какой команды можно добавлять сабмодули в репозиторий?

**Submodule git** — это запись в хост-репозитории git, которая указывает на конкретный коммит в другом внешнем репозитории. Submodule git позволяют содержать один Git-репозиторий как подкаталог другого Git-репозитория. Это даёт возможность клонировать ещё один репозиторий внутрь проекта, храня коммиты для этого репозитория отдельно.

С помощью команды **git submodule add** можно добавить новый подмодуль в существующий репозиторий.

